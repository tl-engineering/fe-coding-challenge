import React, { Component } from 'react';
import Carousel from './components/carousel/carousel'
import ProductInfo from './components/product_info/product_info'
import Description from './components/description/description'

import './styles/App.scss';

class App extends Component {

  state = {
    dataLoaded: false,
    product: {
      id: "",
      title: "",
      body_html: "",
      images: {},
      variants: {}
    }
  };


  componentWillMount() {
    fetch('https://www.mocky.io/v2/5bb4fe093000006600aabc24')
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({product: data.product, dataLoaded: true});
      });
  }

  content() {
    const {images, title, variants, body_html} = this.state.product;
    return (
      <div className="container">
        <div className="row first_row">
          <div className="col-xs col-lg-7 carousel_mobile">
            <Carousel images={images} />
          </div>
          
          <div className="col-xs col-lg-5 product_info">
            <ProductInfo title={title} variants={variants} />
          </div>
        </div>

        <div className="row">
          <Description text={body_html} />
        </div>
      </div>
    );
  }

  // Do not show the app until the json data is loaded
  render() {
    return (
      <div className="App">
        {this.state.dataLoaded ? this.content() : null}
      </div>
    );
  }
}

export default App;