import React from 'react';

import './description.scss'


class Description extends React.Component {


    render() {
        return (
            <div className="col-xs col-md-7 article_description">
                <h2>DETAILS</h2>
                <hr />
                <div dangerouslySetInnerHTML={{ __html: this.props.text}}></div>
            </div>
        );
    }

}

export default Description;