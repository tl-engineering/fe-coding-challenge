import React from 'react';

class ColorPicker extends React.Component {

    pickedColor = (index, e) => {
        let allColors = document.getElementsByClassName("color_swatches");
    
        // Deselect all colors
        [].forEach.call(allColors, function (value) {
            value.classList.remove("selected");
        });
        
        // Select the new color
        e.currentTarget.classList.add("selected");

        // Send the new color to the parent handler
        this.props.updateHandler(e.currentTarget.dataset.value);
    }


    render() {
        const {colors, selected} = this.props;

        return (
            <div className="product_swatches">
                {   
                    Array.from(colors).map( (element, index) => {
                        return (
                            <div 
                                data-value={element}
                                key={index}
                                className={'color_swatches ' + element + ' ' + (selected === element ? "selected" : "")}
                                onClick={(e) => this.pickedColor(index, e)}>
                            </div>
                        )
                    })
                }        
            </div>
        );
    }
}

export default ColorPicker;